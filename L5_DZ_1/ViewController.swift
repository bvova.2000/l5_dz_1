//
//  ViewController.swift
//  L5_DZ_1
//
//  Created by Hen Joy on 4/22/19.
//  Copyright © 2019 Hen Joy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        drawBox()
    }
    
    func drawBox(){
        let aim = UIView()
        aim.frame.size.width = 200
        aim.frame.size.height = 200
        aim.frame.origin.x = 50
        aim.frame.origin.y = 50
        aim.backgroundColor = .cyan
        aim.layer.cornerRadius = aim.frame.size.width/2
        view.addSubview(aim)
        let aimMiddle = UIView()
        aimMiddle.frame.size.width = 150
        aimMiddle.frame.size.height = 150
        aimMiddle.frame.origin.x = 75
        aimMiddle.frame.origin.y = 75
        aimMiddle.backgroundColor = .red
        aimMiddle.layer.cornerRadius = aimMiddle.frame.size.width/2
        view.addSubview(aimMiddle)
        let aimCenter = UIView()
        aimCenter.frame.size.width = 100
        aimCenter.frame.size.height = 100
        aimCenter.frame.origin.x = 100
        aimCenter.frame.origin.y = 100
        aimCenter.backgroundColor = .cyan
        aimCenter.layer.cornerRadius = aimCenter.frame.size.width/2
        view.addSubview(aimCenter)
    }

}

